---
title: km-scale simulations at MPI-M
subtitle: CPH Retreat | Ringberg | 2024-04-23
format:
  mpim-revealjs:
      width: 1245
      height: 700
      css: slides.css
      navigation-mode: linear
author:
  - name: Lukas Kluft
---

# km-scale simulations at MPI-M {auto-animate=true}

Here, we foucs on simulations that are ...

* available from MPI-M
* not _"archived"_ on tape yet
* accessible through our catalog

# km-scale simulations at MPI-M {auto-animate=true visibility="uncounted"}

Here, we foucs on simulations that are ...

* available from MPI-M
* not _"archived"_ on tape yet
* ~~accessible through our catalog~~

# [easy.gems](https://easy.gems.dkrz.de/)

![](static/easy_gems.png){.img-polaroid}

## Tutorials

![](static/easy_gems_tutorial.png){.img-polaroid}

## Catalog {auto-animate=true}

```{python}
#| echo: true
import intake


cat = intake.open_catalog("https://data.nextgems-h2020.eu/catalog.yaml")
list(cat["ICON"])
```

![](static/ngc4008.png){.img-hidden}

## Catalog {auto-animate=true visibility="uncounted"}

```{.python code-line-numbers="5-6"}
import intake


cat = intake.open_catalog("https://data.nextgems-h2020.eu/catalog.yaml")
ds = cat.ICON.ngc4008.to_dask()
ds.tas.mean("cell").plot()
```

![](static/ngc4008.png)

# [NextGEMS]{.nextgems-heading}

::::{.columns}
:::{.column}
* Consortium consists of 26 institutes (coordinated by MPI-M and ECMWF)
* Perform the first coupled km-scale simulations that span "a climate"
* Organised in development cycles to take user feedback into account
:::

:::{.column width=25%}
![](static/Logo-NextGEMS.png){.img-white}
:::
::::

## [NextGEMS simulations]{.nextgems-heading}

ID | Resolution | Period | Grid | Description
--- | --- | --- | --- | ---
`ngc3028` | 5km/5km | 5.5 yrs | HEALPix | Cycle 3 production
`ngc4005`^1^ | 10km/5km | 5 yrs | HEALPix | Debugging
`ngc4006`^1^ | 10km/5km | 15 yrs | HEALPix | Debugging
`ngc4007`^1^ | 10km/5km | 1.5 yrs | HEALPix | Debugging
`ngc4008` |  10km/5km | 30 yrs | HEALPix | Cycle 4 production

[^1^ Broken. Don't use, except you are Hans]{.tiny}

## [Data access]{.nextgems-heading}

```{.python code-line-numbers="2-3"}
cat = intake.open_catalog("https://data.nextgems-h2020.eu/catalog.yaml")
ds = cat.ICON.ngc4008.to_dask()
ds.tas.mean("cell").plot()
```

![](static/ngc4008.png)

# [EERIE]{.eerie-heading}

:::{.columns}
:::{.column}
* Led by AWI and coordinated jointly with Met Office and the University of Reading
* Perform the first coupled km-scale simulations that span "a climate"
* ... and have more ocean eddies
:::

:::{.column width=25%}
![](static/Logo-EERIE-color-claim){.img-white}
:::
::::

## [EERIE simulations]{.eerie-heading}

ID | Resolution | Period | Grid | Description
--- | --- | --- | --- | ---
`erc1011` | 10km/5km | 30 yrs | ICON | Hist./Scenario
`erc1017` | 10km/5km | 55 yrs | ICON | Historical

## [Data access]{.eerie-heading}

EERIE data is subdivided into various datasets

```{.python code-line-numbers="2-3"}
cat = intake.open_catalog("https://data.nextgems-h2020.eu/catalog.yaml")
ds = cat.ICON.erc1017.atmos.native["2d_monthly_mean"].to_dask()
ds.tas.isel(time=slice(1, None)).mean("ncells").plot()
```

![](static/erc1017.png)

# [DestinE]{.destine-heading}

::::{.columns}
:::{.column}
* Implemented by ECMWF, ESA, EUMETSAT (and unnamed contractors)
* Building a highly accurate digital twin of the Earth
:::

:::{.column width=25%}
![](static/Logo-Destination-Earth-Colours.svg){.img-white}
:::
::::

## [DestinE simulations]{.destine-heading}

ID | Resolution | Period | Grid | Description
--- | --- | --- | --- | ---
_it's complicated_ | 10km/5km | 16.5 yrs | HEALPix | Historical
_it's complicated_ | 5km/5km | 10 yrs | HEALPix | Scenario

## [Data access]{.destine-heading}

[Request](https://pad.gwdg.de/ZwCjzlF3Te2swjWFFFCG5w) data from the FDB on the data bridge

```{.python}
import earthkit


request = {
    'activity': 'CMIP6',
    'class': 'd1',
    'dataset': 'climate-dt',
    'date': '19910301',
    'experiment': 'hist',
    'expver': '0001',
    'generation': '1',
    'levtype': 'sfc',
    'model': 'ICON',
    'param': '167',
    'realization': '1',
    'resolution': 'high',
    'stream': 'clte',
    'time': '0100',
    'type': 'fc'

}

#data is an earthkit streaming object but with stream=False will download data immediately 
data = earthkit.data.from_source(
    "polytope",
    "destination-earth",
    request,
    address="polytope.apps.lumi.ewctest.link",
    stream=False,
)
```

## [Data access]{.destine-heading visibility="uncounted"}

```{.python}
style = earthkit.maps.Style(
    levels=range(250, 305, 1),
    units='kelvin',
    extend='both',
)
earthkit.maps.quickplot(data, style=style)
```

![](static/climate-dt.png)

# [C5]{.eurohpc-heading}

::::{.columns}
:::{.column}
* **C**loud-**C**irculation **C**oupling in a **C**hanging **C**limate
* Hierarchy of AMIP simulations at different horizontal resolutions
* ... and in different background climates <br> (historical, +4K, 4x CO~2~)
:::

:::{.column width=5%}
![](static/Logo-EuroHPC-JU.png){.img-white}
:::
::::

## [C5 simulations]{.eurohpc-heading}

ID | Resolution | Period | Grid | Description
--- | --- | --- | --- | ---
`C5.AMIP_CNTL`^1^ | 10km | 15 yrs | HEALPix | Historical
`C5.AMIP_P4K`^1^ | 10km | 15 yrs | HEALPix | _T_~s~ +4K
`C5.AMIP_4XCO2`^2^ | 10km | 15 yrs | HEALPix | 4x CO~2~

[^1^ Useful if you are Hans?]{.tiny}
[^2^ Not yet transferred]{.tiny}

## [Data access]{.eurohpc-heading}

```{.python code-line-numbers="2-3"}
cat = intake.open_catalog("https://data.nextgems-h2020.eu/catalog.yaml")
ds = cat.ICON.C5.AMIP_CNTL.to_dask()
ds.tas.mean("cell").plot()
```

![](static/C5_AMIP_CNTL.png)

# Conclusion

:::{.incremental}
* ICON simulations are available in **various configurations** (un-/coupled, historical/scenario)
* **Analysis-ready** access with Python on Levante
* If a committee is responsible for the design, it [will]{.nextgems-heading} [be]{.eerie-heading} [blue]{.destine-heading}
:::
